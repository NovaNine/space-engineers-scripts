// Instructions:
// Name LCDs "LCD - Cargo Level"
// To ignire some cargos put "#NO-CARGO-LEVEL#" in their CustomData
// For best results set LCD to: Monospace font, font size 1, padding 2

private string IGNORE_TAG = "#NO-CARGO-LEVEL#";
private Color BGERR = new Color(255, 0, 0);
private Color BGALE = new Color(255, 136, 0);
private Color BGNOR = new Color(0,0,0);
private Color FGERR = new Color(255, 255, 0);
private Color FGALE = new Color(0,0,0);
private Color FGNOR = new Color(255,255,255);

public Program()

{

  Runtime.UpdateFrequency = UpdateFrequency.Update100;
}



public void Save()

{

    // Called when the program needs to save its state. Use
    // this method to save your state to the Storage field
    // or some other means. 
    // 
    // This method is optional and can be removed if not
    // needed.

}



public void Main(string argument, UpdateType updateSource)

{
  // Get LCDs
  var lcds = new List<IMyTerminalBlock>();
  GridTerminalSystem.SearchBlocksOfName("LCD - Cargo Level", lcds);
  if (lcds.Count==0) return;

  // Get all cargo container blocks
  var containers = new List<IMyTerminalBlock>(); 
  GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(containers); 

  // declare variables for calculating volume
  VRage.MyFixedPoint curVol = 0; 
  VRage.MyFixedPoint maxVol = 0; 

  // loop through containers
  int num=0;
  for (int i = 0; i < containers.Count; i++) {
    // get the inventory of each container
    // blocks have inventories, even cargo containers are not themselves inventories
    IMyCargoContainer container = containers[i] as IMyCargoContainer; 
    if (container.CustomData.Contains(this.IGNORE_TAG)) continue;
    IMyInventory containerInventory = container.GetInventory(0); 

    // increment volume variables
    curVol += containerInventory.CurrentVolume; 
    maxVol += containerInventory.MaxVolume;
    num++;
  } 
  // calculate % fill
  double pct = ((double)curVol / (double)maxVol) * 100;

  // rounds the volume values for clean display
  double dCurVol = Math.Round((double)curVol, 0);
  double dMaxVol = Math.Round((double)maxVol, 0);

  // Display
  for (int i=0; i < lcds.Count; i++) {
    var lcd = (lcds[i] as IMyTextPanel);
    // Set BG color
    if (pct > 90) { lcd.BackgroundColor = this.BGERR; lcd.FontColor = this.FGERR; }
    else if (pct > 80) { lcd.BackgroundColor = this.BGALE; lcd.FontColor = this.FGALE; }
    else { lcd.BackgroundColor = this.BGNOR; lcd.FontColor = this.FGNOR; }

    lcd.WriteText("CARGO   " + pct.ToString("0.00") + "%   " + num.ToString() + " ct\n\n" +
      this.getBar(pct, 20) + "\n" +
      dCurVol.ToString() + " / "  + dMaxVol.ToString() + " Kl\n\n"
//+ "\nABCDEjF" + "\n" + 
    );
  }
}

private string getBar(double pct, int chars) {
  if (chars==0) return "DIV-BY-0";
  double unit = 100/chars;
  int whole = (int)(pct/unit);
  return new String('+', whole) + new String('-', chars-whole);
}
